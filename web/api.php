<?php

$priceList = [
    [
        'title' => "Free",
        'info' => "Detalii pachet free",
        'price' => "0 $"
    ],
    [
        'title' => "Business",
        'info' => "Detalii pachet business",
        'price' => "100 $"
    ]
];

header("Content-Type' => application/json");

echo json_encode($priceList);
