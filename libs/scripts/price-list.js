function PriceList(){
    this.getPriceList = function(){
        var self = this;
        $.ajax({
            url: '/api.php',
            type: 'post',
            dataType: 'json'
        }).done(function(ajaxRequest){
            self.populate(ajaxRequest);
        });
    };

    this.getTemplate = function(){
        return $('#price-template').html();
    };

    this.populate = function(priceList){
        var tpl = this.getTemplate();

        priceList.forEach(function(item){
            $('#price-list').append(tpl.formatString(item));
        });
    };

    this.show = function(){
        this.getPriceList();
    };
}
