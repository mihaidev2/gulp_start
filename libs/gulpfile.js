'use strict';

var gulp = require('gulp');
var compileScssToCss = require('gulp-sass');
var concat = require('gulp-concat');


gulp.task('css:external', function(){
    return gulp.src([
        'node_modules/bootstrap/dist/css/bootstrap.css',
        'node_modules/bootstrap/dist/css/bootstrap-theme.css'
    ])
        .pipe(concat('external.css'))
        .pipe(gulp.dest('web/css/'));
});

gulp.task('css:local', function(){
    return gulp.src('libs/scss/index.scss')
        .pipe(compileScssToCss())
        .pipe(concat('local.css'))
        .pipe(gulp.dest('web/css/'));
});

gulp.task('scss', ['css:local', 'css:external']);


gulp.task('js:external', function(){
    return gulp.src([
        'node_modules/jquery/dist/jquery.js',
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'node_modules/kisphp-format-string/src/format-string.js'
    ])
        .pipe(concat('external.js'))
        .pipe(gulp.dest('web/js'));
});

gulp.task('js:local', function(){
    return gulp.src([
        'libs/scripts/price-list.js',
        'libs/scripts/index.js'
    ])
        .pipe(concat('local.js'))
        .pipe(gulp.dest('web/js'));
});

gulp.task('js', ['js:external', 'js:local']);

gulp.task('default', ['scss', 'js']);

gulp.task('watch', function(){
    gulp.watch([
            // files to watch
            'libs/scss/**/*.scss'
        ],
        [
            // gulp tasks to run on files change
            'css:local'
        ]
    );
    gulp.watch([
            // files to watch
            'libs/scripts/**/*.js'
        ],
        [
            // gulp tasks to run on files change
            'js:local'
        ]
    );
});
